/*
 *  Copyright (c) 2014, Parse, LLC. All rights reserved.
 *
 *  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
 *  copy, modify, and distribute this software in source code or binary form for use
 *  in connection with the web services and APIs provided by Parse.
 *
 *  As with any software that integrates with the Parse platform, your use of
 *  this software is subject to the Parse Terms of Service
 *  [https://www.parse.com/about/terms]. This copyright notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.parse.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment for the user login screen.
 */
public class ParseLoginFragment extends ParseLoginFragmentBase {

  public interface ParseLoginFragmentListener {
    public void onSignUpClicked(String username, String password);

    public void onLoginHelpClicked();

    public void onLoginSuccess();
  }

  private static final String LOG_TAG = "ParseLoginFragment";
  private static final String USER_OBJECT_NAME_FIELD = "name";
  private static final String USER_OBJECT_FBID_FIELD = "fbid";

  private View parseLogin;

  private Button facebookLoginButton;
  private ParseLoginFragmentListener loginFragmentListener;
  private ParseOnLoginSuccessListener onLoginSuccessListener;

  private ParseLoginConfig config;

  public static ParseLoginFragment newInstance(Bundle configOptions) {
    ParseLoginFragment loginFragment = new ParseLoginFragment();
    loginFragment.setArguments(configOptions);
    return loginFragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                           Bundle savedInstanceState) {
    config = ParseLoginConfig.fromBundle(getArguments(), getActivity());

    View v = inflater.inflate(R.layout.com_parse_ui_parse_login_fragment,
        parent, false);
    v.setBackgroundColor(getResources().getColor(android.R.color.background_dark));
    ImageView appLogo = (ImageView) v.findViewById(R.id.app_logo);
    parseLogin = v.findViewById(R.id.parse_login);

    facebookLoginButton = (Button) v.findViewById(R.id.facebook_login);



    if (appLogo != null && config.getAppLogo() != null) {
      appLogo.setImageResource(config.getAppLogo());
    }

    if (allowFacebookLogin()) {
      setUpFacebookLogin();
    }

    return v;
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    if (activity instanceof ParseLoginFragmentListener) {
      loginFragmentListener = (ParseLoginFragmentListener) activity;
    } else {
      throw new IllegalArgumentException(
          "Activity must implemement ParseLoginFragmentListener");
    }

    if (activity instanceof ParseOnLoginSuccessListener) {
      onLoginSuccessListener = (ParseOnLoginSuccessListener) activity;
    } else {
      throw new IllegalArgumentException(
          "Activity must implemement ParseOnLoginSuccessListener");
    }

    if (activity instanceof ParseOnLoadingListener) {
      onLoadingListener = (ParseOnLoadingListener) activity;
    } else {
      throw new IllegalArgumentException(
          "Activity must implemement ParseOnLoadingListener");
    }
  }

  @Override
  protected String getLogTag() {
    return LOG_TAG;
  }

  private LogInCallback facebookLoginCallbackV4 = new LogInCallback() {
    @Override
    public void done(ParseUser user, ParseException e) {
      if (isActivityDestroyed()) {
        return;
      }

      if (user == null) {
        loadingFinish();
        if (e != null) {
          showToast(R.string.com_parse_ui_facebook_login_failed_toast);
          debugLog(getString(R.string.com_parse_ui_login_warning_facebook_login_failed) +
                  e.toString());
        }
      } else if (user.isNew()) {
        GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                  @Override
                  public void onCompleted(
                      JSONObject fbUser,
                      GraphResponse response) {
                  /*
                    If we were able to successfully retrieve the Facebook
                    user's name, let's set it on the fullName field.
                  */
                    ParseUser parseUser = ParseUser.getCurrentUser();
                    if (fbUser != null && parseUser != null
                            && fbUser.optString("name").length() > 0) {
                      parseUser.put(USER_OBJECT_NAME_FIELD, fbUser.optString("name"));
                      parseUser.put(USER_OBJECT_FBID_FIELD, fbUser.optString("fbId"));
                      parseUser.put("FacebookId", fbUser.optString("id"));
                      parseUser.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                          if (e != null) {
                            debugLog(getString(
                                    R.string.com_parse_ui_login_warning_facebook_login_user_update_failed) +
                                    e.toString());
                          }
                          loginSuccess();
                        }
                      });
                    }
                    loginSuccess();
                  }
                }).executeAsync();


        GraphRequest.newMyFriendsRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONArrayCallback() {
                  @Override
                  public void onCompleted(
                          JSONArray friendlist,
                          GraphResponse response) {
                    List<String> FriendsId = new ArrayList<String>();

                      try{
                      for (int i = 0; i < friendlist.length(); i++) {
                        JSONObject item = friendlist.getJSONObject(i);

                        String id = item.optString("id");
                        FriendsId.add(id);

                      }
                      }catch (JSONException e){
                        JSONObject item = new JSONObject();

                    }

                    ParseUser parseUser = ParseUser.getCurrentUser();
                    parseUser.put("friendlist", FriendsId);
                    parseUser.saveInBackground();
                  }
                }).executeAsync();



      } else if (user.getString("FacebookId") == null || user.getString("FacebookId").length() == 0){
        GraphRequest.newMeRequest(
            AccessToken.getCurrentAccessToken(),
            new GraphRequest.GraphJSONObjectCallback() {
                  @Override
                  public void onCompleted(JSONObject fbuser, GraphResponse graphResponse) {
                    ParseUser.getCurrentUser().put("FacebookId", fbuser.optString("fbid"));
                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        if (e != null) {
                          Log.e("FBID", "Couldn't get user fbid");
                        }
                        loginSuccess();
                      }
                    });
                    loginSuccess();
                  }
        }).executeAsync();
      } else {
        loginSuccess();
      }
    }
  };

  private void setUpFacebookLogin() {
    facebookLoginButton.setVisibility(View.VISIBLE);

    if (config.getFacebookLoginButtonText() != null) {
      facebookLoginButton.setText(config.getFacebookLoginButtonText());
    }

    facebookLoginButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        loadingStart(false); // Facebook login pop-up already has a spinner
        if (config.isFacebookLoginNeedPublishPermissions()) {
          ParseFacebookUtils.logInWithPublishPermissionsInBackground(getActivity(),
                  config.getFacebookLoginPermissions(), facebookLoginCallbackV4);
        } else {
          ParseFacebookUtils.logInWithReadPermissionsInBackground(getActivity(),
                  config.getFacebookLoginPermissions(), facebookLoginCallbackV4);
        }
      }
    });
  }



  private boolean allowFacebookLogin() {
    if (!config.isFacebookLoginEnabled()) {
      return false;
    }

    if (facebookLoginButton == null) {
      debugLog(R.string.com_parse_ui_login_warning_disabled_facebook_login);
      return false;
    } else {
      return true;
    }
  }

  private void loginSuccess() {
    onLoginSuccessListener.onLoginSuccess();
  }

}

/*
 *  Copyright (c) 2014, Parse, LLC. All rights reserved.
 *
 *  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
 *  copy, modify, and distribute this software in source code or binary form for use
 *  in connection with the web services and APIs provided by Parse.
 *
 *  As with any software that integrates with the Parse platform, your use of
 *  this software is subject to the Parse Terms of Service
 *  [https://www.parse.com/about/terms]. This copyright notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.parse.ui;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.util.Log;

import com.parse.Parse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Configurations for the ParseLoginActivity.
 */
public class ParseLoginConfig {
  public static final String APP_LOGO = "com.parse.ui.ParseLoginActivity.APP_LOGO";
  public static final String FACEBOOK_LOGIN_ENABLED = "com.parse.ui.ParseLoginActivity.FACEBOOK_LOGIN_ENABLED";
  public static final String FACEBOOK_LOGIN_BUTTON_TEXT = "com.parse.ui.ParseLoginActivity.FACEBOOK_LOGIN_BUTTON_TEXT";
  public static final String FACEBOOK_LOGIN_PERMISSIONS = "com.parse.ui.ParseLoginActivity.FACEBOOK_LOGIN_PERMISSIONS";


  // For internally serializing to/from string array (the public analog above is for resource from activity meta-data).
  private static final String FACEBOOK_LOGIN_PERMISSIONS_STRING_ARRAY = "com.parse.ui.ParseLoginActivity.FACEBOOK_LOGIN_PERMISSIONS_STRING_ARRAY";

  private static final String LOG_TAG = "com.parse.ui.ParseLoginConfig";

  // Use boxed types so that we can differentiate between a setting not set,
  // versus its default value.  This is useful for merging options set from code
  // with options set by activity metadata.
  private Integer appLogo;


  private Boolean facebookLoginEnabled;
  private CharSequence facebookLoginButtonText;
  private Collection<String> facebookLoginPermissions;


  public Integer getAppLogo() {
    return appLogo;
  }

  public void setAppLogo(Integer appLogo) {
    this.appLogo = appLogo;
  }




  public boolean isFacebookLoginEnabled() {
    if (facebookLoginEnabled != null) {
      return facebookLoginEnabled;
    } else {
      return false;
    }
  }

  public void setFacebookLoginEnabled(boolean facebookLoginEnabled) {
    this.facebookLoginEnabled = facebookLoginEnabled;
  }

  public CharSequence getFacebookLoginButtonText() {
    return facebookLoginButtonText;
  }

  public void setFacebookLoginButtonText(CharSequence facebookLoginButtonText) {
    this.facebookLoginButtonText = facebookLoginButtonText;
  }

  public Collection<String> getFacebookLoginPermissions() {
    if (facebookLoginPermissions != null) {
      return Collections.unmodifiableCollection(facebookLoginPermissions);
    } else {
      return null;
    }
  }

  public void setFacebookLoginPermissions(Collection<String> permissions) {
    if (permissions != null) {
      facebookLoginPermissions = new ArrayList<String>(permissions.size());
      facebookLoginPermissions.addAll(permissions);
    }
  }

  /* package */ boolean isFacebookLoginNeedPublishPermissions() {
    if (facebookLoginPermissions != null) {
      return facebookLoginPermissions.contains("publish_actions") ||
          facebookLoginPermissions.contains("publish_pages");
    } else {
      return false;
    }
  }


  public Bundle toBundle() {
    Bundle bundle = new Bundle();

    if (appLogo != null) {
      bundle.putInt(APP_LOGO, appLogo);
    }



    if (facebookLoginEnabled != null) {
      bundle.putBoolean(FACEBOOK_LOGIN_ENABLED, facebookLoginEnabled);
    }
    if (facebookLoginButtonText != null) {
      bundle.putCharSequence(FACEBOOK_LOGIN_BUTTON_TEXT,
          facebookLoginButtonText);
    }
    if (facebookLoginPermissions != null) {
      bundle.putStringArray(FACEBOOK_LOGIN_PERMISSIONS_STRING_ARRAY,
          facebookLoginPermissions.toArray(new String[0]));
    }



    return bundle;
  }

  /**
   * Constructs a ParseLoginConfig object from a bundle. Unrecognized keys are
   * ignored.
   * <p/>
   * This can be used to pass an ParseLoginConfig object between activities, or
   * to read settings from an activity's meta-data in Manefest.xml.
   *
   * @param bundle
   *     The Bundle representation of the ParseLoginConfig object.
   * @param context
   *     The context for resolving resource IDs.
   * @return The ParseLoginConfig instance.
   */
  public static ParseLoginConfig fromBundle(Bundle bundle, Context context) {
    ParseLoginConfig config = new ParseLoginConfig();
    Set<String> keys = bundle.keySet();

    if (keys.contains(APP_LOGO)) {
      config.setAppLogo(bundle.getInt(APP_LOGO));
    }


    if (keys.contains(FACEBOOK_LOGIN_ENABLED)) {
      config.setFacebookLoginEnabled(bundle.getBoolean(FACEBOOK_LOGIN_ENABLED));
    }
    if (keys.contains(FACEBOOK_LOGIN_BUTTON_TEXT)) {
      config.setFacebookLoginButtonText(bundle.getCharSequence(FACEBOOK_LOGIN_BUTTON_TEXT));
    }
    if (keys.contains(FACEBOOK_LOGIN_PERMISSIONS) &&
        bundle.getInt(FACEBOOK_LOGIN_PERMISSIONS) != 0) {
      // Only for converting from activity meta-data.
      try {
        config.setFacebookLoginPermissions(stringArrayToCollection(context
            .getResources().getStringArray(
                bundle.getInt(FACEBOOK_LOGIN_PERMISSIONS))));
      } catch (NotFoundException e) {
        if (Parse.getLogLevel() <= Parse.LOG_LEVEL_ERROR) {
          Log.w(LOG_TAG, "Facebook");
        }
      }
    } else if (keys.contains(FACEBOOK_LOGIN_PERMISSIONS_STRING_ARRAY)) {
      // For converting from a bundle produced by this class's toBundle()
      config.setFacebookLoginPermissions(stringArrayToCollection(bundle
          .getStringArray(FACEBOOK_LOGIN_PERMISSIONS_STRING_ARRAY)));
    }

    return config;
  }

  private static Collection<String> stringArrayToCollection(String[] array) {
    if (array == null) {
      return null;
    }
    return Arrays.asList(array);
  }
}

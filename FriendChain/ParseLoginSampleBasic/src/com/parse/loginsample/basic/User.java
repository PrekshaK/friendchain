// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;


import android.util.Log;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gaoy on 7/14/15.
 */
@ParseClassName("User")
public class User extends ParseUser {

    final String TAG = "User";
    ParseUser mParseUser = ParseUser.getCurrentUser();
//    List<ParseObject> mFriendEvents = new ArrayList<>();
//    List<Event> mInterestedEvents = new ArrayList<>();
//    List<Event> mAttendingEvents = new ArrayList<>();
    //List<ParseObject> mEventsIAmAttending = new ArrayList<>();
    List<Event> AttendingEventList = new ArrayList<Event>() {
    };

    public User() {
    }

    /*public void getFriendEvents() {
        ParseQuery<ParseObject> friendEventQuery = ParseQuery.getQuery("Event")
                .whereEqualTo("FriendsOfOwnerIds", mParseUser.getObjectId());
        //query.whereEqualTo("AttendingUserIds", mParseUser.getObjectId());
        friendEventQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    Log.d("friendeventQuery", "retrieved " + list.size() + " events");
                    mFriendEvents = list == null ? new ArrayList<ParseObject>() : list;

                } else {
                    Log.d("friendeventQuery", "Error: " + e.getMessage());
                }
            }
        });
    }


    //public void getFriendEvents() {
        List<ParseUser> FriendList = (List<ParseUser>) mParseUser.getList("friendlist");

        Log.d("list2", FriendList.toString());
        ParseQuery<Event> eventQuery = ParseQuery.getQuery("Event");
        for (Event x : FriendList) {
            eventQuery.whereEqualTo("CreatedBy", x);
            eventQuery.findInBackground(new FindCallback<Event>() {
                @Override
                public void done(List<Event> list, ParseException e) {
                    if (e == null) {
                        Log.d("eventQuery", "Retrieved " + list.size() + " events");
                        mFriendEvents.addAll(list);
                    } else {
                        Log.e("eventQuery", "Error: " + e.getMessage());
                    }
                }
            });
        }
    }*/

    /*public void getInterestedEvents() {
        ParseQuery<ParseObject> interestedQuery = ParseQuery.getQuery("Event")
                .whereEqualTo("InterestedUserIds", mParseUser.getObjectId());
        //query.whereEqualTo("AttendingUserIds", "UtNwBdjARr");
        interestedQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    Log.d("interestedQuery", "Retrieved " + list.size() + " events");
                    mInterestedEvents = list == null ? new ArrayList<ParseObject>() : list;

                } else {
                    Log.d("interestedQuery", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void getInterestedEvents() {
        mInterestedEvents = mParseUser.getList("EventsInterested");
    }

    public List<ParseObject> getAttendingEvents() {
        ParseQuery<ParseObject> attendingQuery = ParseQuery.getQuery("Event")
                .whereEqualTo("AttendingUserIds", mParseUser.getObjectId());
        //query.whereEqualTo("AttendingUserIds", "UtNwBdjARr");
        attendingQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    Log.d("attendingQuery", "Retrieved " + list.size() + " events");
                    for (ParseObject parseObj : list) {
                        System.out.println(parseObj.getList("AttendingUserIds"));
                        for (String user: parseObj.<String>getList("AttendingUserIds")) {
                        // go through array of attendees
                        // look for ParseUser.getCurrent()
                        // save to a list of events

                        };
                    }
                    mAttendingEvents = list == null ? new ArrayList<ParseObject>() : list;


                } else {
                    Log.d("attendingQuery", "Error: " + e.getMessage());
                }
            }

        });
        return mAttendingEvents;
    }

    public List<ParseObject> getEventsIAmAttending(){
        ParseQuery<ParseObject> IAmAttending = ParseQuery.getQuery("Event");

        List list = new ArrayList<>();
        IAmAttending.whereEqualTo("EventsAttending", list);
        IAmAttending.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                mEventsIAmAttending = list;

            }
        });
        Log.d("list1", mEventsIAmAttending.toString());
        return mEventsIAmAttending;

    }

/*
    public void getAttendingEvents() {

        mAttendingEvents = (List<Event>) mParseUser.get("EventsAttending");
        /*List<ParseObject> pEvents = mParseUser.getList("EventsAttending");
        if (pEvents != null) {
            for (ParseObject x : pEvents) {
                mAttendingEvents.add(new Event(x));
            }
        }*/




//    public void putInterested(Event event) {
//        mParseUser.add("EventsInterested", event);
//        mParseUser.saveInBackground();
//        removeEventFromFriendList(event);
//    }
//
//    public void removeEventFromFriendList(Event event) {
//        mParseUser.removeAll("FriendEvents", Arrays.asList(event));
//        mParseUser.saveInBackground();
//    }
//
//    public void putAttending(Event event) {
//        //error:operation operation. Cannot add and remove.
//        final Event event1 = event;
//        Log.d(TAG, "EventsInterested: " + mParseUser.get("EventsInterested"));
//        mParseUser.add("EventsAttending", event);
//        //mParseUser.saveInBackground();
//        mParseUser.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                if (e == null) {
//                    mParseUser.removeAll("EventsInterested", Arrays.asList(event1));
//                    mParseUser.saveInBackground();
//                } else {
//                    Log.d("operation2", "sad");
//                }
//            }
//        });
//    }

}

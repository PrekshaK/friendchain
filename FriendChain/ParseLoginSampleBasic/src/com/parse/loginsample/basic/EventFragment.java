// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.facebook.messenger.MessengerUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.google.maps.android.PolyUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emstaple on 7/28/15.
 */
public class EventFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private UserAdapter mAdapter;

    private ParseObject mEvent;
    private TextView mTitle;
    private TextView mDate;
    private TextView mLocation;
    private TextView mDetails;
    private DataManager mDataManager;
    private Handler mHandler;
    private String mListType;
    private GoogleApiClient mGoogleApiClient;

    private RecyclerView mInterested;
    private TextView mNameTextView;
    private Button mInviteButton;
    private Button mSendButton;
    private SupportMapFragment map;
    private ProfilePictureView mProfilePictureView;
    private Callbacks mCallbacks;
    private LatLng mLatLng;
    private boolean mapIsShown = true;
    private ParseUser bindedUser;
    private List<LatLng> mLatLngs;
    private LatLng mLastLatLng;
    private int invited;
    private Button mcheatButton;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("Directions", mLatLngs.toString());
        googleMap.addPolyline(new PolylineOptions()
                .addAll(mLatLngs).width(10).color(Color.parseColor("#66A3FF")));
        googleMap.addMarker(new MarkerOptions().position(mLastLatLng));
        googleMap.addMarker(new MarkerOptions().position(mLatLng));
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(mLastLatLng).include(mLatLng);
        for (LatLng x : mLatLngs) {
            builder.include(x);
        }
        LatLngBounds bounds = builder.build();
        int width = ((WindowManager) getActivity()
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
        int height = ((int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 275, getResources().getDisplayMetrics()));
        Log.d("MapDem", "Height = " + Integer.toString(height)
                + " Width = " + Integer.toString(width));
        /*googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                bounds, width, height, (width - height) * 2 / 3));*/
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
                bounds, width, height, (width - height) * 2 / 3));
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (lastLocation == null){
            FragmentManager manager = getFragmentManager();
            locationErrorDialog dialog = new locationErrorDialog();
            dialog.show(manager, "Error Message");

        }else {
            mLastLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

            new DirectionsFetcher().execute();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public interface Callbacks {
        void onTitleSelected();
    }

    public static EventFragment newInstance(ParseObject event) {
        EventFragment fragment = new EventFragment();
        fragment.mEvent = event;
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
        invited = 0;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.event_fragment, container, false);

        mDataManager = DataManager.getInstance(getActivity());
        mHandler = new Handler(Looper.getMainLooper());
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.event_map);

        if (mEvent != null) {
            mLatLng = new LatLng(mEvent.getParseGeoPoint("Location").getLatitude(),
                    mEvent.getParseGeoPoint("Location").getLongitude());

            mTitle = (TextView) v.findViewById(R.id.dialog_event_title_text_view);
            mTitle.setText(mEvent.getString("Title"));
            mTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallbacks.onTitleSelected();
                }
            });

            mcheatButton = (Button) v.findViewById(R.id.cheat_button);
            mcheatButton.setText("Cheat");
            mcheatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDataManager.makeUsOwners(mEvent);
                }
            });



            mDate = (TextView) v.findViewById(R.id.dialog_event_date_text_view);
            mDate.setText(mDataManager.getReadableDate(mEvent.getDate("testDate")));

            mLocation = (TextView) v.findViewById(R.id.dialog_event_location_text_view);
            mLocation.setText(mEvent.getString("Address"));

            mDetails = (TextView) v.findViewById(R.id.dialog_event_details_text_view);
            mDetails.setText(mEvent.getString("Description"));

            mInterested = (RecyclerView) v.findViewById(R.id.event_dialog_recyclerView);
            mInterested.setLayoutManager(new LinearLayoutManager(getActivity()));

            v.findViewById(R.id.showMap).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mapIsShown) {
                        getFragmentManager().beginTransaction().show(map).commit();
                        mapIsShown = true;
                    } else {
                        getFragmentManager().beginTransaction().hide(map).commit();
                        mapIsShown = false;
                    }
                }
            });
        }

        updateList();

        return v;
    }

    public void updateList() {
        List<ParseUser> list;
        if (ParseUser.getCurrentUser().getString("FacebookId").equals(
                mEvent.getString("OwnerFBID"))) {// && invited < 3) {
            list = mEvent.getList("InterestedUsers");
            mListType = "Interested";
        } else {
            list = mEvent.getList("AttendingUsers");
            mListType = "Attending";
        }

        if(mAdapter == null) {
            mAdapter = new UserAdapter(list);
            mInterested.setAdapter(mAdapter);
        } else {
            mAdapter.setEvents(list);
            mInterested.setAdapter(mAdapter);
        }
    }

    private class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public UserHolder(View itemView) {
            super(itemView);
            mNameTextView = (TextView) itemView.findViewById(R.id.user_name);
            mProfilePictureView = (ProfilePictureView) itemView
                    .findViewById(R.id.user_profile_picture);
            mInviteButton = (Button) itemView.findViewById(R.id.invite_button);
            mSendButton = (Button) itemView.findViewById(R.id.fb_send_button);
            itemView.setOnClickListener(this);
        }

        public void bindUser(final ParseUser user) {
            bindedUser = user;
            mNameTextView.setText(mDataManager.getName(user));
            mProfilePictureView.setProfileId(user.getString("FacebookId"));
            if (mListType.equals("Attending")) {
                mInviteButton.setVisibility(View.GONE);
                mSendButton.setVisibility(View.VISIBLE);
                mSendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        MessengerUtils.openMessengerInPlayStore(getActivity());

                        // TODO: Open messenger to a new compose window to send a message
                    }
                });
            } else {
                mSendButton.setVisibility(View.GONE);
                mInviteButton.setVisibility(View.VISIBLE);
                mInviteButton.setOnClickListener(new View.OnClickListener() {

                    //

                    @Override
                    public void onClick(View v) {
                        invited++;
                        switch (invited) {
                            case 1:
                                List<ParseObject> attendinglist =
                                        mEvent.getList("AttendingUsers");
                                Integer intquota = mEvent.getInt("Quota");
                                if (attendinglist.size() == intquota - 1) { //End of chain
                                    mEvent.put("OwnerFBID", null);
                                    mDataManager.addAttending(mEvent, ParseUser.getCurrentUser());
                                    mDataManager.addAttending(mEvent, user);
                                    invited = 4;
                                    return;
                                }
                                mEvent.put("owner1", user);
                            case 2:
                                mEvent.put("owner2", user);
                            case 3:
                                mEvent.put("owner3", user);
                                // Must save before trying to save current owner, otherwise you
                                // can't get owner1 to set to the new current owner
                                mEvent.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        mDataManager.makeOwner(mEvent,
                                                mEvent.getParseUser("owner1"), null);
                                    }
                                });
                        }
                        updateList();
                    }
                });
            }
            Log.d("ProfilePicture", "User: " + user.get("fbid"));
        }

        @Override
        public void onClick(View v) {

            Log.e("ASDF", "onClick detected");
            Intent intent = null;
            if (isFacebookInstalled()) {
                final String urlFb = "https://www.facebook.com/" + bindedUser.getString("FacebookId");
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlFb));
                Log.e("opening", "this");
                Log.e("ASDF", "facebook is installed and we're opening " + urlFb);
            } else {
                // Open the app store to install facebook
                // Or open a web link to the facebook profile
                Log.e("ASDF", "facebook is NOT installed");
//                        intent.setData(Uri.parse(user));
            }

            if (intent != null) {
                startActivity(intent);
            }

            // TODO: Open facebook to the selected user's profile
        }
    }

    private class UserAdapter extends RecyclerView.Adapter<UserHolder> {

        private List<ParseUser> mUsers;

        public UserAdapter(List<ParseUser> users) {
            mUsers = users;
        }

        @Override
        public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater
                    .from(getActivity());
            View view = layoutInflater.inflate(
                    R.layout.user_listview_fragment, parent, false);
            return new UserHolder(view);
        }

        @Override
        public void onBindViewHolder(UserHolder holder, int position) {
            ParseUser user = mUsers.get(position);
            holder.bindUser(user);
        }

        @Override
        public int getItemCount() {
            return mUsers.size();
        }

        public void setEvents(List<ParseUser> users) {
            mUsers = users;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.d("GoogleApiClient", "Build Started");
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean isFacebookInstalled() {
        return isPackageInstalled("com.facebook.katana") ||
                isPackageInstalled("com.facebook.wakizashi");
    }

    private boolean isPackageInstalled(String packageName) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(packageName, 0);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private class DirectionsFetcher extends AsyncTask<URL, Integer, String> {

        private List<LatLng> latLngs = new ArrayList<>();


        @Override
        protected String doInBackground(URL... params) {
            try {
                HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(
                        new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    }
                });

                Address address = new Geocoder(getActivity())
                        .getFromLocation(mLastLatLng.latitude, mLastLatLng.longitude, 1).get(0);
                String destination = String.format(
                        "%s  %s, %s",
                        address.getAddressLine(0),
                        address.getLocality(),
                        address.getAdminArea());
                GenericUrl url = new GenericUrl("http://maps.googleapis.com/maps/api/directions/json");
                url.put("origin", destination);
                url.put("destination", mEvent.getString("Address"));
                url.put("sensor", false);

                HttpRequest request = requestFactory.buildGetRequest(url);
                HttpResponse httpResponse = request.execute();
                DirectionsResult directionsResult = httpResponse.parseAs(DirectionsResult.class);
                String encodedPoints = directionsResult.routes.get(0).overviewPolyLine.points;
                latLngs = PolyUtil.decode(encodedPoints);
                //return encodedPoints;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            mLatLngs = latLngs;
            startMap();
        }
    }

    public static class DirectionsResult {
        @Key("routes")
        public List<Route> routes;
    }

    public static class Route {
        @Key("overview_polyline")
        public OverviewPolyLine overviewPolyLine;
    }

    public static class OverviewPolyLine {
        @Key("points")
        public String points;
    }

    private void startMap() {
        map.getMapAsync(this);
    }
}

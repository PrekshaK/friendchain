// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DataManager implements Comparator<ParseObject> {
    private static DataManager mDataManager;
    private AsyncTask<Void, Void, Void> mFetchTask;
    private ParseUser mUser;
    private ParseInstallation mInstallation;
    private Date mLastEventSeenTime;
    private List<ParseUser> mActiveFriendList = new ArrayList<>();
    private List<ParseObject> mPendingEvents = new ArrayList<>();
    private List<ParseObject> mAttendingEvents = new ArrayList<>();
    private static List<ParseUser> FRIENDLIST = new ArrayList<>();
    private static List<ParseObject> mEventsIOwn= new ArrayList<>();

    private final SharedPreferences mSharedPreferences;
    private Drawable mProfilePicture;
    private static Geocoder mGeocoder;
    private EventListComparator mComparator = new EventListComparator();


    public static DataManager getInstance(Context context) {

        if (mDataManager == null) {
            mDataManager = new DataManager(context);
        }
        return mDataManager;
    }

    private DataManager(Context context) {
        mAttendingEvents = new ArrayList<>();
        mLastEventSeenTime  = new Date(100, 1, 1);
        mInstallation = ParseInstallation.getCurrentInstallation();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mGeocoder = new Geocoder(context, Locale.getDefault());

        mFetchTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                fetchAll();
                return null;
            }
        };

        // See if we can get the logged-in user object. This may not be possible until we log in.
        ParseUser user = ParseUser.getCurrentUser();
        init(user);

        mUser.put("currentInstallation", mInstallation);
        mActiveFriendList = mUser.getList("friendlist");
    }

    public void init(ParseUser user) {
        if (user == null) {
            return;
        }
        mUser = user;
    }

    private void fetchActiveFriendList() {
        mActiveFriendList = mUser.getList("friendlist");
    }

    public void fetchAttendingEventList() {
        ParseQuery<ParseObject> attendingEventQuery = ParseQuery.getQuery("Event")
                .whereEqualTo("AttendingUsers", mUser)
                .whereGreaterThan("testDate", new Date());
        attendingEventQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null && list != null) {
                    Log.d("attendingEventQuery", "retrieved " + list.size() + "events");
                    Collections.sort(list, mComparator);
                    mAttendingEvents = list == null ? new ArrayList<ParseObject>(): list;
                    //mUser.put("EventsAttending", mAttendingEvents);
                    Log.d("AttendingEventList", mAttendingEvents.toString());
                } else {
                    Log.d("AttendingeventQuery", "Error: " + e.getMessage());
                    mAttendingEvents = new ArrayList<ParseObject>();
                }
            }
        });


//        List<ParseObject> x = mUser.getList("EventsAttending");
//        Collections.sort(x, mComparator);
//        mAttendingEvents = x;
//        Log.d("EventList", mUser.getList("EventsAttending").toString());
    }

    public void fetchPendingEventList() {

        ParseQuery<ParseObject> friendEventQuery = ParseQuery.getQuery("Event")
                .whereContainedIn("OwnerFBID", mActiveFriendList)
                .whereGreaterThan("createdAt", mLastEventSeenTime);
        //query.whereEqualTo("AttendingUserIds", mParseUser.getObjectId());
        friendEventQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null && list != null) {
                    if (list.size() > 0) {
                        Log.d(
                                "pendingeventquery",
                                "retrieved " + list.get(0).getCreatedAt() + " events "
                                        + list.size());
                    }

                    mPendingEvents = list == null ? new ArrayList<ParseObject>() : list;
                    Log.d("lasteventseen", mLastEventSeenTime.toString());

//                    Date newdate = mPendingEvents.get(0).getDate("createdAt");

                    Log.d("mPendingEvents,", mPendingEvents.toString());
//                    //ParseUser.getCurrentUser().put("FriendEvents", list);
//                    //}

                } else {
                    Log.d("pendingeventQuery", "Error: " + e.getMessage());
                    mPendingEvents = new ArrayList<ParseObject>();
                }
            }
        });
    }

    public void fetchEventsIOwn() {
        ParseQuery<ParseObject> eventsIOwn = ParseQuery.getQuery("Event")
                .whereEqualTo("OwnerFBID", ParseUser.getCurrentUser().getString("FacebookId"));
        eventsIOwn.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null && list != null) {
                    if (list.size() > 0) {
                        Log.d("OwnedEvents", "retrieved " +
                                list.get(0).getCreatedAt() + " events " + list.size());
                        Collections.sort(list, mComparator);
                        mEventsIOwn = list;
                    }
                }
            }
        });
    }

    public void fetchAll() {
        //fetchActiveFriendList();
        fetchAttendingEventList();
        fetchPendingEventList();
        fetchEventsIOwn();
    }

//SETTERS

    public void setLastEventSeenTime(Date time) {
        mLastEventSeenTime = time;
    }

//GETTERS

    public ParseUser getUser() {
        if (mUser != null) {
            return mUser;
        } else {
            Log.d("mUser", "null");
            return ParseUser.getCurrentUser();
        }
    }

    public List<ParseUser> getActiveFriendList() {
        if (mActiveFriendList != null) {
            return mActiveFriendList;
        } else {
            Log.d("ActiveFriendList", "null");
            return new ArrayList<>();
        }
    }

    public List<ParseObject> getPendingEventList() {
        if (mPendingEvents != null) {
            return mPendingEvents;
        } else {
            Log.d("PendingEvents", "null");
            return new ArrayList<>();
        }
    }

    public List<ParseObject> getAttendingEventList() {
        if (mAttendingEvents != null) {
            return mAttendingEvents;
        } else {
            Log.d("AttendingFriendList", "null");
            return new ArrayList<>();
        }
    }

    public Date getLastEventSeenTime() {
        if (mLastEventSeenTime != null) {
            return mLastEventSeenTime;
        } else {
            return new Date(100,1,1);
        }
    }

    public List<ParseObject> getEventsIOwn() {
        return mEventsIOwn;
    }

//WRITERS
    //------------------

    //local
    public void removeFromPendingList(ParseObject event) {
        mPendingEvents.remove(event);
        System.out.println("createdAt" + event.getCreatedAt());
        mSharedPreferences.edit()
                .putLong("removedFromPending", getLongFromDate(event.getCreatedAt()))
                .commit();
    }

    //create a column InterestedUsers
    public void addInterestedToEvent(ParseObject event, final OnDoneCallback callback) {
        event.add("InterestedUsers", mUser);
        event.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (callback != null) {
                    callback.onDone();
                }
                if (e != null) {
                    System.out.println(e);
                }
            }
        });
    }

    //create a column AttendingUsers
    public void addAttending(final ParseObject event, final ParseUser user) {
        event.addUnique("AttendingUsers", user);
        event.saveInBackground();
//        new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                if (e == null) {
//                    event.add("AttendingUsers", user);
//                    event.saveInBackground(new SaveCallback() {
//                        @Override
//                        public void done(ParseException e) {
//                            if (callback != null) {
//                                callback.onDone();
//                                System.out.println("callback done");
//                            }
//                            if (e != null) {
//                                System.out.println(e);
//                            }
//                        }
//                    });
//                }
//            }
//        });
        if (event.getObjectId() != null) {
            setChannels(event);
        }

    }

    public void removeInterestedfromEvent(ParseObject event, final OnDoneCallback callback) {
        event.removeAll("InterestedUsers", Arrays.asList(mUser));
        event.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (callback != null) {
                    callback.onDone();
                }
            }
        });
    }

    //create a column Owner
    public void makeOwner(ParseObject event, ParseUser user, final OnDoneCallback callback) {

        String id = user.getString("FacebookId");
        event.put("OwnerFBID", id);
        Log.e("ownershipexpirationtime", setOwnershipExpirationTime().toString());
        event.put("ownershipExpirationTime", setOwnershipExpirationTime());
//        event.put("owner1", user);
//        event.put("owner2", user);
//        event.put("owner3", user);
        event.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (callback != null) {
                    callback.onDone();
                }
            }
        });
    }

    public void setChannels(final ParseObject event) {
        ParsePush.subscribeInBackground("Event" + event.getObjectId(), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d(
                        "com.parse.push",
                        "successfully subscribed to the broadcast channel"
                            + event.getObjectId());
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }


    public void setEventDetails(
            String title,
            Date date,
            String description,
            ParseGeoPoint location,
            String address,
            String stringDate,
            Integer quota,
            ParseObject event) {
        event.put("Title", title);
        event.put("testDate", date);
        event.put("Location", location);
        event.put("Address" , address);
        event.put("Date", stringDate);
        event.put("Description", description);
        event.put("Quota", quota);
        event.put("InterestedUsers", new ArrayList<>());
       // makeUsOwners(event);
        event.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("setEvent", "saved");
                    System.out.println("savedSetEvent");
                } else {
                    System.out.println(e);
                }
            }
        });
    }

    @Override
    public int compare(ParseObject lhs, ParseObject rhs) {
        return lhs.getCreatedAt().compareTo(rhs.getCreatedAt());
    }

    public boolean checkQuotaMet(ParseObject event) {
        if (event == null) {
            return false;
        }
        if (event.getList("AttendingUsers").size() < (Integer) event.get("Quota")) {
            return false;
        } else {
            event.add("AttendingUsers", event.get("Owner"));
            event.put("OwnerFBID", null);
            return true;
        }
    }

    public boolean checkEventPassed(ParseObject event) {
        if (event == null) {
            return false;
        }
        if (event.getDate("testDate").before(new Date())) {
            mUser.removeAll("EventsAttending", Arrays.asList(event));
            try {
                event.delete();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }

    }

    public void setIndex(ParseObject event, Integer i) {
        if (i > 2) {
            return;
        } else {
            event.put("currentOwnerIndex", i);
        }
    }


//OTHER
    //----------------------------------

    public interface OnDoneCallback {
        void onDone();
    }


    public static List<ParseUser> getFriendsfromId(List<Object> friendlistarray) {
        ParseQuery friendQuery = ParseUser.getQuery();
        friendQuery.whereContainedIn("FacebookId", friendlistarray);
        try {
            FRIENDLIST = friendQuery.find();
        } catch (ParseException e) {
            Log.d("Friendlist: ", "could not be fetched");

        }
        return FRIENDLIST;

    }


    public Long getLongFromDate(Date date) {

//        SimpleDateFormat f = new SimpleDateFormat("dd-MMM-yyyy");
//        String string_date = f.format(date);
//        Date d = null;
//        try {
//            d = f.parse(string_date);
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//        long milliseconds = d.getTime();
//        return milliseconds;
        return date.getTime();
    }

    public Date getDateFromLong(Long newLong) {
        Date d = new Date(newLong);
        return d;
    }

//    public Bitmap getProfilePicture(String fbid) {
//        mProfilePicture = gerpropic(fbid);
//        return mProfilePicture;
//    }


    public Drawable gerpropic(String fbid) {
        final String FBID = fbid;

        final String userFacebookId = FBID;

        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                // safety check
                if (userFacebookId == null)

                    return null;

                String url = String.format(
                        "https://graph.facebook.com/%s/picture?width=999999",
                        userFacebookId);

                // you'll need to wrap the two method calls
                // which follow in try-catch-finally blocks
                // and remember to close your input stream
                try {
                    InputStream inputStream = new URL(url).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    return bitmap;
                } catch (IOException e) {
                    Log.d("sorry", "i am a bad person");
                    return null;
                }

            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                // safety check
                if (bitmap != null) {

                    float scaleFactorwidth = (float) 2;
                    float scaleFactorheight = (float) 2;

                    int sizeX = Math.round(bitmap.getWidth() * scaleFactorwidth);
                    int sizeY = Math.round(bitmap.getHeight() * scaleFactorheight);

                    Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap, sizeX, sizeY, false);


                    Canvas newCanvas = new Canvas(bitmapResized);

                    newCanvas.drawBitmap(bitmapResized, 0, 0, null);

                    String captionString = getNameFromId(userFacebookId);

                    Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paintText.setColor(Color.BLACK);
                    paintText.setTextSize(200);
                    paintText.setStyle(Paint.Style.FILL);
                    paintText.setShadowLayer(10f, 10f, 10f, Color.BLACK);

                    Rect rectText = new Rect();
                    paintText.getTextBounds(captionString, 0, captionString.length(), rectText);

                    newCanvas.drawText(
                            captionString,
                            0,
                            rectText.height(),
                            paintText);


                    mProfilePicture = new BitmapDrawable(null, bitmapResized);


                }
            }

        }.execute();
        return mProfilePicture;
    }

    public String getNameFromId(String Id){
        ParseQuery<ParseUser> nameQuery = ParseUser.getQuery();
        nameQuery.whereMatches("FacebookId", Id);
        ParseUser name;
        try {
             name = nameQuery.getFirst();
        }catch(ParseException e){
            name = null;


        }
        return  name.getString("name");
    }

    public String getReadableDate(Date date) {
        String d = (new SimpleDateFormat("EEEE, MMMM d").format(date)) +
                getDateSuffix(date) + (new SimpleDateFormat("h:mm a ").format(date));
        return d;
    }

    private String getDateSuffix(Date date) {
        int day = Integer.parseInt(new SimpleDateFormat("d", Locale.getDefault()).format(date));
        switch (day) {
            case 1: case 21: case 31:
                return ("st at ");

            case 2: case 22:
                return ("nd at ");

            case 3: case 23:
                return ("rd at ");

            default:
                return ("th at ");
        }
    }

    public String getReadableAddress(ParseGeoPoint location) {
        Address address = new Address(Locale.getDefault());
        try {
            List addressList = mGeocoder.getFromLocation(
                    location.getLatitude(), location.getLongitude(), 1);
            if (addressList.size() > 0) {
                address = (Address) addressList.get(0);
                return String.format(
                    "%s  %s, %s",
                    address.getAddressLine(0),
                    address.getLocality(),
                    address.getAdminArea());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Could not find location";

    }

    public Date setOwnershipExpirationTime(){
        Long time = new Date().getTime();
        time = time + (6000*11*15);
        Date date = new Date(time);
        return date;

    }

    public String getName(ParseUser user) {
        try {
            return user.fetchIfNeeded().getString("name");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "Name not found";
    }

    public void makeUsOwners(ParseObject Event){
       List<ParseUser> friendlistid =  getFriendsfromId(ParseUser.getCurrentUser().getList("friendlist"));

       // Arrays.asList(friendlistid);
        Event.put("owner1", ParseUser.getCurrentUser());
        Event.put("owner2", friendlistid.get(0));
        Event.put("owner3", friendlistid.get(1));
        Event.saveInBackground();

    }
}

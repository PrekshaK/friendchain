// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by puku on 8/3/15.
 */
public class simpleDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity()).setTitle("Empty Field")
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }
}

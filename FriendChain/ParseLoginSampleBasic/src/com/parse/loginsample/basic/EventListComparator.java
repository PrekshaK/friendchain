// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.Comparator;

/**
 * Created by emstaple on 7/29/15.
 */
public class EventListComparator implements Comparator<ParseObject> {
    @Override
    public int compare(ParseObject lhs, ParseObject rhs) {
        try {
            lhs = lhs.fetchIfNeeded();
            rhs = rhs.fetchIfNeeded();
            return rhs.getCreatedAt().compareTo(lhs.getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
//    @Override
//    public int compare(final ParseObject lhs, final ParseObject rhs) {
//        lhs.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
//            @Override
//            public void done(ParseObject parseObject, ParseException e) {
//                rhs.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
//                    @Override
//                    public void done(ParseObject parseObject, ParseException e) {
//                        saving = false;
//                    }
//                });
//            }
//        });
//        while(saving) {
//            //do nothing
//        }
//        return rhs.getCreatedAt().compareTo(lhs.getCreatedAt());
//    }
}

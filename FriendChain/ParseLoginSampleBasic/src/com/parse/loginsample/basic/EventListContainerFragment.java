// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by emstaple on 7/28/15.
 */
public class EventListContainerFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.events_tab_fragment_container, container, false);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = new EventListFragment();
        ft.add(R.id.event_list, fragment, "EventListFragment").commit();

        return v;
    }
}

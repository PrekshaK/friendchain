// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.andtinder.model.CardModel;
import com.andtinder.view.CardContainer;
import com.andtinder.view.SimpleCardStackAdapter;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.Date;
import java.util.List;

/**
 * Created by emstaple on 7/6/15.
 */
public class ChooseEventFragment extends Fragment implements ViewPageFragment,CardModel.OnCardDimissedListener {

    private static final String TAG = ChooseEventFragment.class.getSimpleName();
    private static final int LOGIN_REQUEST = 0;
    private static final String OBJECT_ID_KEY = "objectId";
    private static final String USER_FB_ID_KEY = "authData";
    private static final String USER_FRIEND_EVENT_LIST_KEY = "friendList";
    private static final String EVENT_OBJECT = "Event";
    private static final String USER_NAME_KEY = "name";

    private DataManager mDataManager;
    private ParseObject mEvent;
    private ParseUser mUser;
    private Button mInterestedButton;
    private Button mDeclineButton;
    private List<ParseObject> mPendingEvents;
    private List<ParseObject> mAttendingEvents;
    private List<ParseObject> mInterestedEvents;
    private Button mLogoutButton;
    private Button mCreateInterestTestButton;
    private Button mCreateAttendingTestButton;
    private Button mCreateEventTestButton;
    private Button mNextEventTestButton;
    private CardContainer mCardContainer;
    private CardModel card;
    private CardModel cardy;
    private boolean isrealcard = false;
    private  CardModel card1;
    ParseObject currentevent;
    private SpringListener mSpringListener;
    private Spring mSpring;
    //private EventAdapter adapter;

    Handler mHandler;

    private TextView NameField;
    private TextView EventNameField;
    private TextView LocationField;
    private TextView DateField;
    private TextView DescripField;
    private int y = 0;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mDataManager = DataManager.getInstance(getActivity());
//        mDataListener = new DataManager.Listener() {
//            @Override
//            public void onDataUpdate() {
//                bindData();
//            }
//        };
        mHandler = new Handler(Looper.getMainLooper());
        /*parseData = new ParseData();
        mFriendEvents = new ArrayList<>();
*/
        mSpring = SpringSystem.create().createSpring();
        mSpringListener = new ButtonSpringListener();
        mSpring.setAtRest()
                .setEndValue(0)
                .setCurrentValue(0)
                .setOvershootClampingEnabled(false)
                .setSpringConfig(new SpringConfig(180, 10))
                .addListener(mSpringListener);

        bindData();
        updateEventList();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void bindData() {
        mUser = mDataManager.getUser();
        mPendingEvents = mDataManager.getPendingEventList();
        mAttendingEvents = mDataManager.getAttendingEventList();
    }

//    private class EventHolder implements SimpleAdapter.ViewBinder {
//
//        private ParseObject mEvent;
//
//        private TextView mTitleTextView;
//        private TextView mDateTextView;
//        private TextView mLocationTextView;
//
//        public EventHolder(View itemView) {
//            super(itemView);
//            itemView.setOnClickListener(this);
//
//            mTitleTextView = (TextView) itemView.findViewById(
//                    R.id.list_item_event_title_text_view);
//            mDateTextView = (TextView) itemView.findViewById(
//                    R.id.list_item_event_date_text_view);
//            mLocationTextView = (TextView) itemView.findViewById(
//                    R.id.list_item_event_location_text_view);
//        }
//
//        @Override
//        public boolean setViewValue(View view, Object data, String textRepresentation) {
//
//            return false;
//        }
//    }
//
//    private class EventAdapter extends SimpleAdapter {
//        public EventAdapter(Context context, List<? extends Map<String, ?>> data,
//                            int resource, String[] from, int[] to) {
//            super(context, data, resource, from, to);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState) {
        View v = inflater.inflate(R.layout.explorer_fragment, container, false);
        mCardContainer = (CardContainer) v.findViewById(R.id.layoutview);
        updateUI();

        /*SwipeFlingAdapterView flingContainer = (SwipeFlingAdapterView) v.findViewById(R.id.frame);
        //ArrayAdapter adapter = new ArrayAdapter<ParseObject>(getActivity(), R.layout.event_listview_fragment, mAttendingEvents);
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
//        List<String> titles = new ArrayList<>();
//        List<String> locations = new ArrayList<>();
//        List<String> dates = new ArrayList<>();

        for (ParseObject x : mAttendingEvents) {
            map.put("Title", x.getString("Title"));
            map.put("Date", x.getString("Date"));
            map.put("Location", x.getString("Address"));
        }
        list.add(map);

        adapter = new EventAdapter(getActivity(), list,
                R.layout.event_listview_fragment, new String[]{"Title", "Date", "Location"},
                new int[]{R.id.list_item_event_title_text_view, R.id.list_item_event_date_text_view,
                R.id.list_item_event_location_text_view});
        //adapter.setViewBinder(new EventHolder(v.findViewById(R.id.container)));
        flingContainer.setAdapter(adapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object o) {

            }

            @Override
            public void onRightCardExit(Object o) {

            }

            @Override
            public void onAdapterAboutToEmpty(int i) {

            }

            @Override
            public void onScroll(float v) {

            }
        });*/

        /*mCreateEventTestButton = (Button) v.findViewById(R.id.test_button_create);
        mCreateEventTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //final ProgressDialog dialog = makeProgressDialog();
                //if (parseData == null) {
                //parseData = new ParseData();
                //mFriendEvents = parseData.mFriendEvents;

                final ParseObject eventObject = new com.parse.loginsample.basic.Event();
                //eventObject.put("Title", "test");
                Date currentDate = new Date();
                //MIGHT CAUSE OPERATIONOPERATION ERROR---------------->
                mDataManager.setEventDetails(
                        "Title",
                        new Date(currentDate.getTime() + 60000 * 15),
                        "Description",
                        new ParseGeoPoint(10, 10),
                        "My place",
                        "Sometime",
                        10,
                        eventObject);
                mDataManager.addAttending(eventObject, mUser);
                mDataManager.makeOwner(eventObject, mUser, null);
                mDataManager.setIndex(eventObject, 0);
                eventObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            mDataManager.setChannels(eventObject);
                        } else {
                            Log.d("channels", "error");
                        }
                    }
                });
                //final ParseUser currentUser = ParseUser.getCurrentUser();

                //<------------------

                //mDataManager.addFriendEvents(eventObject);
                //currentUser.add("EventsAttending", eventObject);
                //eventObject.add("FriendsOfOwnerIds", currentUser);
//                currentUser.saveInBackground(new SaveCallback() {
//                    @Override
//                    public void done(ParseException e) {
//                        if (e == null) {
//                            currentUser.add("FriendEvents", eventObject);
//                            currentUser.saveInBackground(); //for testing
//                        } else {
//                            Log.d("operations", "sad");
//                        }
//                    }
//                });

                mNextEventTestButton.setEnabled(true);
                System.out.println(mDataManager.getPendingEventList());
//                try {
//                    mDataManager.fetchData();
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
            }
        });*/

        mNextEventTestButton = (Button) v.findViewById(R.id.next_event_button);

        mNextEventTestButton.setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                mSpring.setEndValue(0.7);
                                return true;
                            case MotionEvent.ACTION_CANCEL:
                            case MotionEvent.ACTION_UP:
                                mSpring.setEndValue(1);
                                return true;
                            default:
                                break;
                        }
                        return false;
                    }
                });
        mNextEventTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(getCurrentEvent() + "nextcurrentevent");
                if (mDataManager.getPendingEventList().size() > 0) {

                    mDataManager.removeFromPendingList(getCurrentEvent());


                    if (getCurrentEvent() == null || mDataManager.getPendingEventList().size() <= 1) {
                        mNextEventTestButton.setEnabled(false);
                    }
                }
            }
        });


        mCreateInterestTestButton = (Button) v.findViewById(R.id.interested_event_button);
        mCreateInterestTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog dialog = makeProgressDialog();

                System.out.println(getCurrentEvent() + "interestedcurrentevent");

                System.out.println("FriendEvents" + mDataManager.getPendingEventList());
                if (mDataManager.getPendingEventList().size() > 0) {
                    DataManager.OnDoneCallback callback =
                            new DataManager.OnDoneCallback() {
                                @Override
                                public void onDone() {
//                                    mHandler.postDelayed(
//                                            new Runnable() {
//                                                public void run() {
                                    dialog.dismiss();
//                                                }
//                                            },
//                                            3000);
                                }
                            };


                    //try {
                    mDataManager.addInterestedToEvent(getCurrentEvent(), callback);
                    mDataManager.removeFromPendingList(getCurrentEvent());
                    //mDataManager.markInterest(getCurrentEvent());
                    //mDataManager.fetchData();
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
                    // TODO: Add user to list of interested users
                    //mDataManager.markInterest(getCurrentEvent());
                }

                if (getCurrentEvent() == null) {
                    mCreateInterestTestButton.setEnabled(false);
                }
            }
        });

        /*mCreateAttendingTestButton = (Button) v.findViewById(R.id.test_button_create_attending);
        mCreateAttendingTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<ParseObject> friendEventsList = mDataManager.getPendingEventList();
                System.out.println("Friend events: " + friendEventsList);
                ParseObject event = getCurrentEvent();
                System.out.println("attendingeventbutton" + event);
//                DataManager.OnDoneCallback callback =
//                        new DataManager.OnDoneCallback() {
//                            @Override
//                            public void onDone() {
//                                dialog.dismiss();
//                            }
//                        };
                if (event != null) {
                    System.out.println("Committing change to attend event: " + event);
                    mDataManager.addAttending(event, mUser);
//                    List<ParseObject> attendingEventsList = mDataManager.getAttendingEvents();
                    //mDataManager.markInterest(getCurrentEvent());
                } else {
                    // WTF?
                    Log.e(TAG, "Unexpected null event on attending button click");
                    mCreateAttendingTestButton.setEnabled(false);
                    //callback.onDone();
                }

            }
        });*/

        updateEventList();
        return v;
    }

    public void updateUI() {
        //adapter.notifyDataSetChanged();
        currentevent = getCurrentEvent();
        SimpleCardStackAdapter adapter = new SimpleCardStackAdapter(getActivity());

        if (currentevent != null) {
            isrealcard = true;
            CardModel card = new CardModel();
            String title = currentevent.getString("Title");
            String Date = currentevent.getString("Date");
            String Address = currentevent.getString("Address");
            Drawable drawable = mDataManager.gerpropic(currentevent.getString("OwnerFBID"));
            card.setCardImageDrawable(drawable);
            card.setTitle(title + Date);
            card.setDescription(Address);

//
//            card = (new CardModel(currentevent.getString("Title") + "                 " +
//                    currentevent.getString("Date"), currentevent.getString("Address"),
//                    mDataManager.gerpropic(currentevent.getString("OwnerFBID"))));


            Log.d("currentevent", currentevent.getString("Title"));

            card.setOnCardDimissedListener(this);
            adapter.add(card);



        } else {
            isrealcard = false;
            cardy = (new CardModel("Sorry                        No events",
                    "You can swipe for more",
                    mDataManager.gerpropic(ParseUser.getCurrentUser().getString("FacebookId"))));
                 //   getResources().getDrawable(R.drawable.ic_launcher)));

            cardy.setOnCardDimissedListener(this);
            adapter.add(cardy);
            if (mDataManager == null) {
                mDataManager = DataManager.getInstance(getActivity());
            } else {
                Log.d("ChooseEventFragment", " DataManager exists");
            }
            mDataManager.setLastEventSeenTime(mDataManager.getDateFromLong(PreferenceManager
                    .getDefaultSharedPreferences(getActivity())
                    .getLong("removedFromPending", new Date(100, 1, 1).getTime())));
            mDataManager.fetchPendingEventList();

        }
       // card1 = (new CardModel("Swipe", "this for more", getResources().getDrawable(R.drawable.together)));
      //  adapter.add(card1);
        mCardContainer.setAdapter(adapter);

    }

    @Override
    public void onLike() {
        if (isrealcard) {
            /// y += 1;
            Log.d("Swipeable Card", "I liked it");
            final ProgressDialog dialog = makeProgressDialog();

            System.out.println(getCurrentEvent() + "interestedcurrentevent");

            System.out.println("FriendEvents" + mDataManager.getPendingEventList());
            if (mDataManager.getPendingEventList().size() > 0) {
                DataManager.OnDoneCallback callback =
                        new DataManager.OnDoneCallback() {
                            @Override
                            public void onDone() {
//                                    mHandler.postDelayed(
//                                            new Runnable() {
//                                                public void run() {
                                dialog.dismiss();
//                                                }
//                                            },
//                                            3000);
                            }
                        };


                //try {
                mDataManager.addInterestedToEvent(getCurrentEvent(), callback);
                mDataManager.removeFromPendingList(getCurrentEvent());

                // TODO: Add user to list of interested users
                //mDataManager.markInterest(getCurrentEvent());
            }
            //   y += 1;
        }
        updateUI();

    }

    @Override
    public void onDislike() {
        if (isrealcard) {
            // y += 1;
                Log.e("removedevent is", currentevent.toString());
                mDataManager.removeFromPendingList(currentevent);
        }
        Log.d("Swipeable Card", "I did not liked it");
      //  y += 1;
        updateUI();
    }



    public ParseObject getCurrentEvent(int y) {
        List<ParseObject> list = mDataManager.getPendingEventList();
        if (list.size() > y) {
            Log.d("eventpending", list.get(y).toString());

            return list.get(y);
        } else {
            mDataManager.fetchPendingEventList();
            y = 0;
            return null;
        }
    }

    public ParseObject getCurrentEvent() {
        List<ParseObject> list = mDataManager.getPendingEventList();
        if (list.size() > 0) {
            ParseObject firstEvent = list.get(0);
            Log.d("eventpending", firstEvent.toString());
            if (!mDataManager.checkEventPassed(firstEvent) &&
                    !mDataManager.checkQuotaMet(firstEvent)) {
                return firstEvent;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private void updateEventList() {
        new FetchItemsTask().execute();
    }

    private ProgressDialog makeProgressDialog() {
        ProgressDialog chooseEventProgressDialog = new ProgressDialog(getActivity());
        chooseEventProgressDialog.setTitle("Please wait ...");
        chooseEventProgressDialog.setMessage("Storing data");
        chooseEventProgressDialog.setIndeterminate(true);
        chooseEventProgressDialog.setCancelable(false);
        chooseEventProgressDialog.setCanceledOnTouchOutside(false);
        chooseEventProgressDialog.show();
        return chooseEventProgressDialog;
    }



    @Override
    public void onViewPageResume() {
        if (mDataManager == null) {
            mDataManager = DataManager.getInstance(getActivity());
        } else {
            Log.d("ChooseEventFragment", " DataManager exists");
        }
        mDataManager.setLastEventSeenTime(mDataManager.getDateFromLong(PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .getLong("removedFromPending", new Date(100, 1, 1).getTime())));
        Log.d("ChooseEventFragment", " page resumed");
    }

    private class FetchItemsTask extends AsyncTask<Void, Void, List<ParseObject>> {

        public FetchItemsTask() {}

        @Override
        protected List<ParseObject> doInBackground(Void... params) {
            return mDataManager.getPendingEventList();
        }

//        @Override
//        protected void onPostExecute(List<ParseObject> list) {
//            mAttendingEvents = list;
//            adapter.notifyDataSetChanged();
//        }
    }

    private class ButtonSpringListener extends SimpleSpringListener {

        @Override
        public void onSpringActivate(Spring spring) {

        }

        @Override
        public void onSpringUpdate(Spring spring) {
            double currentValue = spring.getCurrentValue();
            Log.e("BUGGER", "onSpringUpdate: " + currentValue);
            mNextEventTestButton.setScaleX((float) currentValue);
            mNextEventTestButton.setScaleY((float) currentValue);
        }

        @Override
        public void onSpringAtRest(Spring spring) {

        }
    }
}

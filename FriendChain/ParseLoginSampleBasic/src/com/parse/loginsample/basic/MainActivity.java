/*
*  Copyright (c) 2014, Parse, LLC. All rights reserved.
*
*  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
*  copy, modify, and distribute this software in source code or binary form for use
*  in connection with the web services and APIs provided by Parse.
*
*  As with any software that integrates with the Parse platform, your use of
*  this software is subject to the Parse Terms of Service
*  [https://www.parse.com/about/terms]. This copyright notice shall be
*  included in all copies or substantial portions of the software.
*
*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
*  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
*  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
*  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
*  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*/

package com.parse.loginsample.basic;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.facebook.login.widget.ProfilePictureView;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;

import java.util.Date;
import java.util.List;

/**
* Shows the user profile. This simple activity can function regardless of whether the user
* is currently logged in.
*/
public class MainActivity extends ActionBarActivity implements
        ActionBar.TabListener, EventListFragment.Callbacks, EventFragment.Callbacks,
        AddEventFragment.Callbacks, MapSearchFragment.Callbacks {

    private static final int LOGIN_REQUEST = 0;

    private DataManager mDataManager;
    private ActionBar mActionBar;
    com.parse.loginsample.basic.TabPagerAdapter mTabPagerAdapter;
    private NonSwipeableViewPager mViewPager;
    private String[] tabs = { "Explore", "Events", "Create" };
    private ProfilePictureView mProfilePictureView;
    private boolean isNotificationOff = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().setFormat(PixelFormat.RGBA_8888);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);


        mTabPagerAdapter = new com.parse.loginsample.basic.TabPagerAdapter(getSupportFragmentManager());
        mViewPager = (NonSwipeableViewPager) findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mTabPagerAdapter);

        mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(false);
        setTabs();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                mActionBar.setSelectedNavigationItem(position);
                Fragment fragment = mTabPagerAdapter.getFragmentFromTab(position);
                if (fragment instanceof ViewPageFragment) {
                    ((ViewPageFragment) fragment).onViewPageResume();
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    @Override
    protected void onStart() {
      super.onStart();

        ensureLogin();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDataManager != null) {
            mDataManager.setLastEventSeenTime(mDataManager.getDateFromLong(PreferenceManager
                    .getDefaultSharedPreferences(this)
                    .getLong("removedFromPending", new Date(100, 1, 1).getTime())));
            mDataManager.fetchAll();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == LOGIN_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ensureLogin();
            } else {
                tryLogin();
            }
        }
    }

    private void ensureLogin() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser == null) {
            tryLogin();
        } else {
            mDataManager = DataManager.getInstance(this);
            mDataManager.init(currentUser);
        }
    }

    private void tryLogin() {
        ParseLoginBuilder loginBuilder = new ParseLoginBuilder(MainActivity.this);
        startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(mViewPager.getWindowToken(),
                        InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout, menu);

        MenuItem notificationitem = menu.findItem(R.id.menu_item_notification);
        if (isNotificationOff) {
            notificationitem.setTitle("Turn Notification On");
        } else {
            notificationitem.setTitle("Turn Notification Off");
        }
       return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_logout:
                ParseUser.logOut();
                ParseLoginBuilder loginBuilder=new ParseLoginBuilder(this);
                startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
                return true;

            case R.id.menu_item_facebook:
                final String urlFb = "fb://";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlFb));

                // If Facebook application is installed, use that else launch a browser
                final PackageManager packageManager = getPackageManager();
                List<ResolveInfo> list =
                        packageManager.queryIntentActivities(
                            intent,
                            PackageManager.MATCH_DEFAULT_ONLY);
                if (list.size() == 0) {
                    final String urlBrowser = "https//:www.facebook.com";
                    intent.setData(Uri.parse(urlBrowser));
                }

                startActivity(intent);
                return true;

            case R.id.menu_item_notification:
                invalidateOptionsMenu();
                updateNotification();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateNotification(){
        if (isNotificationOff){
            //turn notification on
            isNotificationOff = false;
        }else{
            //turn notification Off
            isNotificationOff = true;
        }
    }

    @Override
    public void onEventSelected(ParseObject event) {
        Fragment newDetail = EventFragment.newInstance(event);
        FragmentManager fm = getSupportFragmentManager();
        Fragment old = fm.findFragmentByTag("EventListFragment");
        fm.beginTransaction().hide(old).add(R.id.event_list, newDetail, "EventFragment")
                .addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    @Override
    public void onTitleSelected() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment newDetail = fm.findFragmentByTag("EventListFragment");
        Fragment old = fm.findFragmentByTag("EventFragment");
        fm.beginTransaction().remove(old).show(newDetail)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
    }

    @Override
    public void onMapSelected(ParseGeoPoint geoPoint) {
        mActionBar.removeAllTabs();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        mActionBar.setTitle("Location");
        Fragment newDetail = MapSearchFragment.newInstance(geoPoint);
        FragmentManager fm = getSupportFragmentManager();
        Fragment old = fm.findFragmentByTag("AddEventFragment");
        fm.beginTransaction().hide(old).add(R.id.add_event, newDetail, "MapFragment")
                .addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    @Override
    public void onMapClosed(String loc) {
        FragmentManager fm = getSupportFragmentManager();
        AddEventFragment f = (AddEventFragment) fm.findFragmentByTag("AddEventFragment");
        f.updateLocation(loc);
//        Fragment old = fm.findFragmentByTag("MapFragment");
//        fm.beginTransaction().remove(old).show(f)
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
//                .commit();
//        mViewPager.setCurrentItem(2);
        resetTabs();
    }

    private void setTabs() {
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.setDisplayOptions(mActionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_HOME
                | ActionBar.DISPLAY_SHOW_TITLE);
        mActionBar.setTitle(R.string.app_name);
        for (String tab_name : tabs) {
            mActionBar.addTab(mActionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }
    }

    private void resetTabs() {
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.setDisplayOptions(mActionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_HOME
                | ActionBar.DISPLAY_SHOW_TITLE);
        mActionBar.setTitle(R.string.app_name);
        for (String tab_name : tabs) {
            mActionBar.addTab(mActionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }
        mTabPagerAdapter.notifyDataSetChanged();
        mActionBar.setSelectedNavigationItem(2);
        mViewPager.setCurrentItem(2);
    }
}

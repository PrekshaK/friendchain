// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

/**
 * Created by puku on 7/30/15.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class customRecyclerView extends RecyclerView {

    Context context;

    public customRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public customRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public customRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean fling(int velocityX, int velocityY) {

        velocityY *= 0.7;

        return super.fling(velocityX, velocityY);
    }
}

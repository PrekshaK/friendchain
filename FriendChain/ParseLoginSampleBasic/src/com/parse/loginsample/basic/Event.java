// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by gaoy on 7/7/15.
 */

@ParseClassName("Event")
public class Event extends ParseObject implements Comparable<ParseObject> {
    @Override
    public int compareTo(ParseObject another) {
        return this.getDate("createdAt").compareTo(another.getDate("createdAt"));
    }

    public static void sortList(List<ParseObject> events) {
        ParseObject[] stuff = events.toArray(new ParseObject[events.size()]);
        Arrays.sort(stuff);
        events = Arrays.<ParseObject>asList(stuff);
    }
}

// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by emstaple on 7/16/15.
 */
public class  TabPagerAdapter extends FragmentPagerAdapter {

    private Fragment mChooseEventFragment;
    private Fragment mEventListFragment;
    private Fragment mAddEventFragment;
    private FragmentManager mFragmentManager;

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                mChooseEventFragment = new ChooseEventFragment();
                //chooseEventFragmentId = chooseEventFragment.getId();
                return mChooseEventFragment;
            case 1:
                // Games fragment activity
                mEventListFragment = new EventListContainerFragment();
                //eventListFragmentId = eventListFragment.getId();
                return mEventListFragment;
            case 2:
                // Movies fragment activity
                mAddEventFragment = new AddEventContainerFragment();
                //addEventFragmentId = addEventFragment.getId();
                return mAddEventFragment;


        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getFragmentFromTab(int index) {
        switch (index) {
            case 0:
                return mChooseEventFragment;
                //return mFragmentManager.findFragmentById(chooseEventFragmentId);
            case 1:
                return mEventListFragment;
                //return mFragmentManager.findFragmentById(eventListFragmentId);
            case 2:
                return mAddEventFragment;

                //return mFragmentManager.findFragmentById(addEventFragmentId);
        }

        return null;
    }
}

// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

/**
 * Convenience interface to be notified of fragment visibility/activation in a viewpager
 */
public interface ViewPageFragment {
    void onViewPageResume();
}

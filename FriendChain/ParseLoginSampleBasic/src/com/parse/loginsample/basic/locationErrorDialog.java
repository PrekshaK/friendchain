// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

/**
 * Created by puku on 8/3/15.
 */
public class locationErrorDialog extends DialogFragment {
    private Button mButton;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.locationerror, null);
        mButton = (Button) view.findViewById(R.id.turn_location_on);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });

        return new AlertDialog.Builder(getActivity()).setTitle("Turn Your Location On")
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .create();

    }



}

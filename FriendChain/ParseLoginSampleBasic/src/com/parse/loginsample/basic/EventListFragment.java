// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by emstaple on 7/9/15.
 */
public class EventListFragment extends Fragment implements ViewPageFragment {

    private RecyclerView mEventRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private EventAdapter mAdapter;
    private DataManager mDataManager;
    private User user = new User();
    private Switch mSwitch;
    private List<ParseObject> events;
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onEventSelected(ParseObject event);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user.add("EventsInterested", new com.parse.loginsample.basic.Event());
        mDataManager = DataManager.getInstance(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.event_recyclerview,
                container,
                false);
        mSwitch = (Switch) view.findViewById(R.id.switch1);
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    buttonView.setText("Attending");
                    updateUI();
                } else {
                    buttonView.setText("Currently Owned");
                    updateUI();
                }
            }
        });

        mEventRecyclerView = (RecyclerView) view.findViewById(
                R.id.event_recyclerview);
        mEventRecyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity()));

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });

        refreshItems();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }


    public void updateUI() {
        if (mSwitch.isChecked()){
           events = mDataManager.getEventsIOwn();
        } else {
            events = mDataManager.getAttendingEventList();
        }

        if (events == null){
            events = new ArrayList<>();
            mAdapter.notifyDataSetChanged();
            Log.d("events", "events was null");
            return;
        }

        if(mAdapter == null) {
            mAdapter = new EventAdapter();
            mEventRecyclerView.setAdapter(mAdapter);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewPageResume() {
        if (mDataManager == null) {
            mDataManager = DataManager.getInstance(getActivity());
        } else {
            Log.d("ChooseEventFragment", " DataManager exists");
        }
        mDataManager.setLastEventSeenTime(mDataManager.getDateFromLong(PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .getLong("removedFromPending", new Date(100, 1, 1).getTime())));
        updateUI();
        Log.d("ChooseEventFragment", " page resumed");
    }

    private class EventHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private ParseObject mEvent;

        private TextView mTitleTextView;
        private TextView mDateTextView;
        private TextView mLocationTextView;

        public EventHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            mTitleTextView = (TextView) itemView.findViewById(
                    R.id.list_item_event_title_text_view);
            mDateTextView = (TextView) itemView.findViewById(
                    R.id.list_item_event_date_text_view);
            mLocationTextView = (TextView) itemView.findViewById(
                    R.id.list_item_event_location_text_view);
        }

        public void bindEvent(ParseObject event) throws ParseException {
            Log.d("ChooseEventFragment", "Started bind");
            mEvent = event.fetchIfNeeded();
            Log.d("ChooseEventFragment", "Fetched event if needed");
            mTitleTextView.setText(mEvent.getString("Title"));
            Log.d("ChooseEventFragment", "Got title");
            mDateTextView.setText(mEvent.getString("Date"));
            Log.d("ChooseEventFragment", "Set date");
            mLocationTextView.setText(mEvent.getString("Address"));
            Log.d("ChooseEventFragment", "Finished bind event");
        }

        @Override
        public void onClick(View v) {
            Log.d("RecyclerView", "Clicked");
            mCallbacks.onEventSelected(mEvent);
        }
    }

    private class EventAdapter extends RecyclerView.Adapter<EventHolder> {

        @Override
        public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater
                    .from(getActivity());
            View view = layoutInflater.inflate(
                    R.layout.event_listview_fragment, parent, false);
            return new EventHolder(view);
        }

        @Override
        public void onBindViewHolder(EventHolder holder, int position) {
            ParseObject event = events.get(position);
            try {
                holder.bindEvent(event);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.d("ChooseEventFragment", "Binding event");
        }

        @Override
        public int getItemCount() {
            return events.size();
        }
    }

    void refreshItems() {
        final boolean checked = mSwitch.isChecked();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (checked) {
                    mDataManager.fetchEventsIOwn();
                } else {
                    mDataManager.fetchAttendingEventList();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void none) {
                onItemsLoadComplete();
            }
        }.execute();
    }

    void onItemsLoadComplete() {
        updateUI();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}

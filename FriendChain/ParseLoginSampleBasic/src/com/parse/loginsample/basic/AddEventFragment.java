// Copyright 2004-present Facebook. All Rights Reserved.

package com.parse.loginsample.basic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class AddEventFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ViewPageFragment {

    private static final int REQUEST_DATE = 0;

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private static final String API_KEY = "AIzaSyAVumkSOKPNHi_k4bn375DA61iPdwXRoLU";

    private EditText mNameField;
    private TextView mDateButton;
    private TextView mPlaceField;
    private EditText mDetailField;
    private Button mCreateButton;
    private ImageButton mSearchButton;
    private EditText mquota;
    private Integer mQuota;
    private Spinner mspinner;


    private String nameOfEvent;
    private Date dateOfEvent;
    private String detailOfEvent;
    private DataManager mDataManager;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private ParseGeoPoint mGeoPoint = new ParseGeoPoint();
    private LatLng mLatLng;
    private PlacesAutoCompleteAdapter mAdapter;
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onMapSelected(ParseGeoPoint geoPoint);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataManager = DataManager.getInstance(getActivity());

        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle saveInstanceState) {
        View v = inflater.inflate(R.layout.add_event_fragment, container, false);

        mNameField = (EditText) v.findViewById(R.id.add_eventname_textfield);
        mNameField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mNameField.setHint("");
                    mNameField.setCursorVisible(true);
                    ((InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE))
                            .showSoftInput(mNameField, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    mNameField.setHint(R.string.event_name);
                    mNameField.setCursorVisible(false);
                    ((InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(mNameField.getWindowToken(),
                                    InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        mDateButton = (TextView) v.findViewById(R.id.create_event_date_button);
        updateDateButton(new Date());
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getFragmentManager())
                        .setListener(listener)
                        .setInitialDate(dateOfEvent)
                        .setTheme(SlideDateTimePicker.HOLO_DARK)
                        .build()
                        .show();
            }
        });

        mDetailField = (EditText) v.findViewById(R.id.add_details_textfield);

        mAdapter = new PlacesAutoCompleteAdapter(
                getActivity(), android.R.layout.simple_dropdown_item_1line);
        mPlaceField = (TextView) v.findViewById(R.id.add_place_textfield);
        //mPlaceField.setAdapter(mAdapter);


        mspinner = (Spinner) v.findViewById(R.id.spinner_quota);

        List<String> numlist = Arrays.asList(
            "Attendees wanted",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, numlist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mspinner.setAdapter(adapter);




        /*mSearchButton = (ImageButton) v.findViewById(R.id.imageButton);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation(mPlaceField.getText().toString());
            }
        });*/

        mCreateButton = (Button) v.findViewById(R.id.create_button);
        mCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((mNameField.getText().length() < 1) ||
                        (mDetailField.getText().length() < 1) ||
                        Objects.equals(mspinner.getSelectedItem().toString(), "Attendees wanted") ||
                        (mPlaceField.getText().toString().length() < 1)) {
                    FragmentManager manager = getFragmentManager();
                    simpleDialog dialog = new simpleDialog();
                    dialog.show(manager, "Error Message");
                } else {
                    nameOfEvent = mNameField.getText().toString().trim();
                    detailOfEvent = mDetailField.getText().toString().trim();
                    mQuota = Integer.parseInt(mspinner.getSelectedItem().toString());

                    final ParseObject eventObject = new com.parse.loginsample.basic.Event();
                    mDataManager.setEventDetails(
                            nameOfEvent,
                            dateOfEvent,
                            detailOfEvent,
                            mGeoPoint,
                            mPlaceField.getText().toString(),
                            mDateButton.getText().toString(),
                            mQuota,
                            eventObject);
                    ParseUser currentUser = ParseUser.getCurrentUser();
                    mDataManager.addAttending(eventObject, currentUser);
                    mDataManager.makeOwner(eventObject, currentUser, null);
                    mDataManager.setIndex(eventObject,2);
                    eventObject.saveInBackground();

                    ViewPager vp = (ViewPager) getActivity().findViewById(R.id.pager);
                    vp.setCurrentItem(1);
                    resetFields();
                }
            }
        });

        v.findViewById(R.id.map_holder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("thisthing", "clicked");
                mCallbacks.onMapSelected(mGeoPoint);
            }
        });

        mPlaceField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("thisthing", "clicked");
                mCallbacks.onMapSelected(mGeoPoint);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data
                    .getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            updateDateButton(date);
        }
    }

    private void updateDateButton(Date date) {
        mDateButton.setText(mDataManager.getReadableDate(date));
        dateOfEvent = date;
    }

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date)
        {
            updateDateButton(date);
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("Map", CameraUpdateFactory.newLatLng(mLatLng).toString());
        googleMap.clear();
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 12));
        googleMap.addMarker(new MarkerOptions().position(mLatLng));
    }

    protected synchronized void buildGoogleApiClient() {
        Log.d("GoogleApiClient", "Build Started");
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            mGeoPoint.setLatitude(mLastLocation.getLatitude());
            mGeoPoint.setLongitude(mLastLocation.getLongitude());
            mPlaceField.setText(mDataManager.getReadableAddress(mGeoPoint));
            mLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            SupportMapFragment mapFragment = (SupportMapFragment) //this
                    getChildFragmentManager()
                            .findFragmentById(R.id.map);
            Log.d("FragmentId", Integer.toString(R.id.map));
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("GoogleApiClient", connectionResult.toString());
    }

    @Override
    public void onViewPageResume() {
        if (mDataManager == null) {
            mDataManager = DataManager.getInstance(getActivity());
        } else {
            Log.d("AddEventFragment", " DataManager exists");
        }
        mDataManager.setLastEventSeenTime(mDataManager.getDateFromLong(PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .getLong("removedFromPending", new Date(100, 1, 1).getTime())));
        Log.d("AddEventFragment", " page resumed");
    }

    private void resetFields() {
        mDetailField.setText("");
        mNameField.setText("");
        mspinner.setSelection(0);
        updateDateButton(new Date());
        if (mLastLocation != null) {
            mGeoPoint.setLatitude(mLastLocation.getLatitude());
            mGeoPoint.setLongitude(mLastLocation.getLongitude());
            mPlaceField.setText(mDataManager.getReadableAddress(mGeoPoint));
            mLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            SupportMapFragment mapFragment = (SupportMapFragment) //this
                    getChildFragmentManager()
                            .findFragmentById(R.id.map);
            Log.d("FragmentId", Integer.toString(R.id.map));
            mapFragment.getMapAsync(this);
        }
    }

    private ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = new ArrayList<String>();

        try {

            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(
                    new HttpRequestInitializer() {
                        @Override
                        public void initialize(HttpRequest request) {
                            request.setParser(new JsonObjectParser(JSON_FACTORY));
                        }
                    }
            );

            GenericUrl url = new GenericUrl(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            url.put("input", input);
            url.put("key", API_KEY);
            url.put("sensor",false);
            Log.d("ACUrl", url.toString());

            HttpRequest request = requestFactory.buildGetRequest(url);
            HttpResponse httpResponse = request.execute();
            PlacesResult directionsResult = httpResponse.parseAs(PlacesResult.class);

            List<Prediction> predictions = directionsResult.predictions;
            for (Prediction prediction : predictions) {
                resultList.add(prediction.description);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mAdapter.notifyDataSetChanged();
        return resultList;
    }

    public static class PlacesResult {
        @Key("predictions")
        public List<Prediction> predictions;
    }

    public static class Prediction {
        @Key("description")
        public String description;

        @Key("id")
        public String id;
    }

    private class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    }
                    else {
                        notifyDataSetInvalidated();
                    }
                }};
            return filter;
        }
    }

    public void updateLocation(final String newLocation) {
        new AsyncTask<Void, Void, Address>() {
            @Override
            protected Address doInBackground(Void... params) {
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    return geocoder.getFromLocationName(newLocation, 1).get(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Address address) {
                if (address == null) {
                    return;
                }
                mPlaceField.setText(newLocation);
                mGeoPoint.setLatitude(address.getLatitude());
                mGeoPoint.setLongitude(address.getLongitude());
                mLatLng = new LatLng(mGeoPoint.getLatitude(), mGeoPoint.getLongitude());
                updateMap();
                super.onPostExecute(address);
            }
        }.execute();
    }

    private void updateMap() {
        SupportMapFragment mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
}

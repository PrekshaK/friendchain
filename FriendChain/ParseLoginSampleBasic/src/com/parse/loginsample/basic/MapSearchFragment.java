package com.parse.loginsample.basic;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.parse.ParseGeoPoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emstaple on 8/3/15.
 */
public class MapSearchFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private static final String API_KEY = "AIzaSyAVumkSOKPNHi_k4bn375DA61iPdwXRoLU";

    private AutoCompleteTextView mPlaceField;
    private ImageButton mSearchButton;
    private GoogleApiClient mGoogleApiClient;
    private ParseGeoPoint mGeoPoint;
    private LatLng mLatLng;
    private PlacesAutoCompleteAdapter mAdapter;
    private Location mLastLocation;
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onMapClosed(String loc);
    }

    public static MapSearchFragment newInstance(ParseGeoPoint geoPoint) {
        MapSearchFragment fragment = new MapSearchFragment();
        fragment.mGeoPoint = geoPoint;
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState) {
        View v = inflater.inflate(R.layout.map_fragment, container, false);

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        mAdapter = new PlacesAutoCompleteAdapter(
                getActivity(), android.R.layout.simple_dropdown_item_1line);
        mPlaceField = (AutoCompleteTextView) v.findViewById(R.id.map_search);
        mPlaceField.setAdapter(mAdapter);

        mSearchButton = (ImageButton) v.findViewById(R.id.imageButton);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation(mPlaceField.getText().toString());
            }
        });

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 12));
        googleMap.addMarker(new MarkerOptions().position(mLatLng));
    }

    protected synchronized void buildGoogleApiClient() {
        Log.d("GoogleApiClient", "Build Started");
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
//            mGeoPoint.setLatitude(mLastLocation.getLatitude());
//            mGeoPoint.setLongitude(mLastLocation.getLongitude());
            mLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            SupportMapFragment mapFragment = (SupportMapFragment) //this
                    getChildFragmentManager()
                            .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("GoogleApiClient", connectionResult.toString());
    }

    private ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = new ArrayList<String>();

        try {

            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(
                    new HttpRequestInitializer() {
                        @Override
                        public void initialize(HttpRequest request) {
                            request.setParser(new JsonObjectParser(JSON_FACTORY));
                        }
                    }
            );

            GenericUrl url = new GenericUrl(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            url.put("input", input);
            url.put("key", API_KEY);
            url.put("sensor",false);
            Log.d("ACUrl", url.toString());

            HttpRequest request = requestFactory.buildGetRequest(url);
            HttpResponse httpResponse = request.execute();
            PlacesResult directionsResult = httpResponse.parseAs(PlacesResult.class);

            List<Prediction> predictions = directionsResult.predictions;
            for (Prediction prediction : predictions) {
                resultList.add(prediction.description);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mAdapter.notifyDataSetChanged();
        return resultList;
    }

    public static class PlacesResult {
        @Key("predictions")
        public List<Prediction> predictions;
    }

    public static class Prediction {
        @Key("description")
        public String description;

        @Key("id")
        public String id;
    }

    private class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    }
                    else {
                        notifyDataSetInvalidated();
                    }
                }};
            return filter;
        }
    }

    private void updateLocation(final String newLocation) {
        new AsyncTask<Void, Void, Address>() {
            @Override
            protected Address doInBackground(Void... params) {
                Geocoder geocoder = new Geocoder(getActivity());
                try {
                    return geocoder.getFromLocationName(newLocation, 1).get(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Address address) {
                if (address == null) {
                    return;
                }
                mGeoPoint.setLatitude(address.getLatitude());
                mGeoPoint.setLongitude(address.getLongitude());
                mLatLng = new LatLng(mGeoPoint.getLatitude(), mGeoPoint.getLongitude());
                updateMap();
                super.onPostExecute(address);
            }
        }.execute();
    }

    private void updateMap() {
        SupportMapFragment mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCallbacks.onMapClosed(DataManager.getInstance(getActivity())
                .getReadableAddress(mGeoPoint));
    }
}
